/**
 * Created by Badrpas on 11.08.2014.
 */
public class Box extends Physical {
    Box ( float x, float y) {
        super(x, y, 32, 32);
        animations.add(new Animation("idle", "res\\box1.png", 32, 32, 0, 1, 1f));
        animations.add(new Animation("idle2", "res\\box1.png", 16, 16, 0, 1, 1f));
    }
    public void preUpdate() {
        this.setCurrentAnimation("idle");
    }
    public void update() {
        super.update();
    }
    public void render() {
        super.render();
    }
}
