/**
 *   Created by Badrpas on 09.08.2014.
 */
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import java.util.Iterator;

public class Player extends Physical {
    float speed = 300f;
    boolean onGround        = false;
    boolean wasOnGround     = false;
    boolean inAir           = false;
    Player(){
        super(400, 200, 64, 64);
        animations.add(new Animation("run", "res\\manRun.png", 32, 32,  0,11, 25f));
        animations.add(new Animation("idle","res\\manRun.png", 32, 32, 11, 1, 20f));
        animations.add(new Animation("fall","res\\manRun.png", 32, 32, 12, 1, 20f));

        currentAnimation = "run";
        physWidth /= 4f;
        //this.setGravityEnabled(true);
        this.setGravityForce(0, 100f);
    }
    float gravityForceAcceleration = 981.0f;
    public void update() {
        super.update();
        Direction preDir = this.getDirection();

        this.setWasOnGround(this.isOnGround());
        this.setOnGround(false); // check for ground pos;
        this.setInAir(true);
        for (Physical i : this.getCollisions()) {
            if (Math.abs(i.getX() - this.getX()) <  this.getPhysWidth()/2f + i.getPhysWidth()/2f) {
                if (i.getY() > this.getY()) {
                    this.setOnGround(true);
                    this.setInAir(false);
                    this.setCurrentAnimation("idle");
                    i.setCurrentAnimation("idle2");
                }
                if (i.getY() < this.getY()) {
                    this.setForceY(0f);
                }
            }
        }

        if (this.isOnGround()){
            //this.setForceY(0f);
            if (this.isWasOnGround() && Math.abs(this.getForceY()) > 1f/Timer.dt() ) {
                this.setForceY(this.getForceY() / 2f);
            }
        } else {
            currentAnimation = "fall";
            this.addForce(0f, gravityForceAcceleration*Timer.dt());
        }

        this.setForceX(0f); // reset x speed
        //this.setForce(0f, 0f);
        if (Keyboard.isKeyDown(Keyboard.KEY_D)){
            this.setForceX(speed);
            this.setDirection(Direction.Right);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
            this.setForceX(-speed);
            this.setDirection(Direction.Left);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_W)){
            if (this.isOnGround()) {
                this.setForce(0, -speed * 1.5f);
            }
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_S)){
            //this.addForce(0,  speed);
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_D) && Keyboard.isKeyDown(Keyboard.KEY_A)) {
            this.setDirection(preDir);
            this.setForceX(0f);
        }
        if (Mouse.isButtonDown(0)) {
            this.x = Mouse.getX();
            this.y = Display.getHeight() - Mouse.getY();
        }
        if (this.isOnGround() && this.getForceX() != 0f ){
            currentAnimation = "run";
        }
    }
    public void render(){
        super.render();
    }

    public boolean isOnGround() {
        return onGround;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    public boolean isWasOnGround() {
        return wasOnGround;
    }

    public void setWasOnGround(boolean wasOnGround) {
        this.wasOnGround = wasOnGround;
    }

    public boolean isInAir() {
        return inAir;
    }

    public void setInAir(boolean inAir) {
        this.inAir = inAir;
    }

    protected void onCollideWith ( Physical o ) {
        //System.out.println(o.getY() < this.getY()  );
        {
           // o.setCurrentAnimation("idle2");
        }
    }
}
