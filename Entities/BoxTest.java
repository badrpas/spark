import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import java.util.Iterator;

/**
 *  Created by Badrpas on 09.08.2014.
 */
public class BoxTest extends Physical {
    BoxTest() {
        super(Mouse.getX(), Mouse.getY(), 64, 64 );
        animations.add(new Animation( "idle", "res\\box.png", 64, 64, 0, 1, 1f));
        animations.add(new Animation( "villager", "res\\VillagerRun1.png", 128, 128, 91, 13, 20.0f));
//        width = 64;
//        height= 64;
//        this.x = ;
//        this.y = ;
    }

    public void update(){
        super.update();
        this.setForce( Mouse.getX() - x, Display.getHeight() - Mouse.getY() - y);
        if (Mouse.isButtonDown(0)){
            this.x = Mouse.getX();
            this.y = Display.getHeight() - Mouse.getY();
        }
        if (Mouse.isButtonDown(1)){
            System.out.println(this.getCollisions().size());
        }
        animations.get("villager").update();
    }
    public void render() {
        super.render();

        for (Physical i : this.collisions){
            //System.out.println("null");
            animations.get("villager").render(i.getX(), i.getY(), i.getX() + 128f, i.getY() + 128f);
            //animations.get("villager").render( x-width/2, y- height/2, x+width/2, y+height/2 );
        }
    }
}
