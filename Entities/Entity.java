//import org.newdawn.slick.opengl.Texture;

import com.sun.media.sound.RIFFInvalidDataException;

/**
 *  Created by Badrpas on 09.08.2014.
 */
enum Direction
{
    Left,
    Right
}

public class Entity {
    protected static    Scene           currentScene = null;
    protected           boolean         visible;
    protected           Animations      animations;
    protected           String          currentAnimation;
    protected           float           x, y,               // position on screen (center)
                                        width, height;      // size on screen
    protected static    int             updatedCount = 0;
    protected           Direction       direction;

    public Entity() {
        if (currentScene != null)
            currentScene.addEntity(this);
        currentAnimation    = "idle";
        visible             = true;
        animations          = new Animations();
        x                   = 0f;
        y                   = 0f;
        width               = 0f;
        height              = 0f;
    }

    public Entity(float x, float y) {
        this();
        this.x              = x;
        this.y              = y;
        width               = 0f;
        height              = 0f;
    }

    public Entity(float x, float y, float width, float height) {
        this();
        this.x              = x;
        this.y              = y;
        this.width          = width;
        this.height         = height;
    }

    public void setCurrentAnimation(String currentAnimation) {
        if (animations.contains(currentAnimation))
            this.currentAnimation = currentAnimation;
    }

    public void preUpdate(){

    }
    public void update(){
    }
    public void postUpdate(){

    }
    public void render(){
        if (!animations.contains(currentAnimation)) {
            return;
        }
        if (direction == Direction.Right)
            this.animations.get(currentAnimation).setReversedHor(false);
        else
            this.animations.get(currentAnimation).setReversedHor(true);

        if (animations.contains(currentAnimation))
            animations.get(currentAnimation).update();
        animations.get(currentAnimation).render( x-width/2, y- height/2, x+width/2, y+height/2 );
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public static Scene getCurrentScene() {
        return currentScene;
    }

    public static void setCurrentScene(Scene currentScene) {
        Entity.currentScene = currentScene;
    }
}
