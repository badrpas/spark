import org.lwjgl.util.vector.Vector2f;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *  Created by Badrpas on 09.08.14.
 *  Рівняння рівноприскоренного руху
 *  x = x0  + v0x*t + (a*x*t^2)/2
 *  Ріняння швидкості рівноприскоренного руху
 *  v = v0x + ax*t
 *
 */

public class Physical extends Entity{
    protected static Set<Physical>  instances = new HashSet<Physical>();
    protected Set<Physical>         collisions;
    protected float                 physWidth;
    protected float                 physHeight;
    protected Vector2f              force;
    protected Vector2f              gravityForce;
    protected Vector2f              gravityForceInitial;
    protected boolean               solid;
    protected boolean               gravityEnabled;
    static protected int            updatedPhysCount = 0;

    public Physical() {
        super();
        instances.add(this);
        force               = new Vector2f ( 0f, 0f );
        gravityForce        = new Vector2f ( 0f, 981f );
        gravityForceInitial = new Vector2f ( 0f, 981f );
        physWidth           = width;
        physHeight          = height;
        collisions          = new HashSet<Physical>();
        solid               = true;
        gravityEnabled      = false;
    }

    public Physical(float x, float y) {
        super(x, y);
        instances.add(this);
        force               = new Vector2f ( 0f, 0f );
        gravityForce        = new Vector2f ( 0f, 981f );
        gravityForceInitial = new Vector2f ( 0f, 981f );
        physWidth           = width;
        physHeight          = height;
        collisions          = new HashSet<Physical>();
        solid               = true;
        gravityEnabled      = false;

    }


    public Physical(float x, float y, float width, float height) {
        super(x, y, width, height);
        instances.add(this);
        force               = new Vector2f ( 0f, 0f );
        gravityForce        = new Vector2f ( 0f, 981f );
        gravityForceInitial = new Vector2f ( 0f, 981f );
        physWidth           = width;
        physHeight          = height;
        collisions          = new HashSet<Physical>();
        solid               = true;
        gravityEnabled      = false;
    }

    public void update() {
        if (updatedPhysCount == 0) {
            for (Physical instance : instances)
                instance.collisions.clear();
            for ( Physical i : instances ){
                i.physUpdate();
            }
        }
        if (++updatedPhysCount >= instances.size())
            updatedPhysCount = 0;

        super.update();
    }

    void newUpdate(){
        if ( this.force.x != 0.0f )
            if(search(this.x + this.force.x * Timer.dt(), this.y ))
                search(this.x + this.force.x * Timer.dt()/2f, this.y );
        if ( this.force.y != 0.0f )
            if(search(this.x, this.y + this.force.y * Timer.dt() ))
                search(this.x, this.y + this.force.y * Timer.dt()/2f );
    }

    boolean search(float x, float y) { // искомые координаты
        float reservX = this.x;
        float reservY = this.y;
        this.x = x;
        this.y = y;
        boolean isCollide;
        boolean isCollided= false;
        for ( Physical i : instances ) if ( i != this ) {
            float hw = i.physWidth /2f; //      halfWidth
            float hh = i.physHeight/2f; // and  halfHeight
            float hwn= this.physWidth / 2f;
            float hhn= this.physHeight/ 2f;
            isCollide = (
                    Math.abs(this.x - i.x) <= hw + hwn &&
                    Math.abs(this.y - i.y) <= hh + hhn);
            isCollided = isCollided || isCollide;
            if (isCollide){
                this.collisions.add(i);
                i.collisions.add(this);
            }
        }
        if (isCollided){
            this.x = reservX;
            this.y = reservY;
        }
        return isCollided;
    }

    private void physUpdate(){
        if (gravityEnabled) {
            addForce(gravityForce);
        }
        // только при дэбаге
        if (Timer.dt() > 1.0f)
            return;
        newUpdate();
        //calculateMove();
        collisions.remove(this); // прост
//        if (collisions.size() > 0) {
//            onCollide();
//            for (Physical collision : collisions) {
//                onCollideWith(collision);
//            }
//        }
    }

    void calculateMove() {
        float preX = this.x; // запоминаем начальные значения координат
        float preY = this.y;
        this.x += this.force.x * Timer.dt(); // расчитываем икс как он должен быть
        if (check( preX, preY )) { // истина возвращается, если коллизия произошла
            this.x += this.force.x * Timer.dt() / 2f; // еще раз считаем икс только уже на половину меньше
            check(preX, preY);
        }
        this.y += this.force.y * Timer.dt();
        preX = this.x; // Важная строка. Мы провели свои опыты над иксом и не хотели бы потерять их результаты после этого
        if (check( preX, preY )) {
            this.y += this.force.y * Timer.dt() / 2f;
            check(preX, preY);
        }
    }

    boolean check ( float preX, float preY) { // если коллайдит - возвращает позицию на координаты из аргумента

        Iterator<Physical> i = instances.iterator();
        boolean result = false;
        while (i.hasNext()) {
            // чек на коллизию с другим объектом
            Physical o = i.next();
            if ( o.isCollidedWith(this) ) {
                this.x = preX;
                this.y = preY;
                result = true;
                this.collisions.add(o);
                o.collisions.add(this);
            }
        }
        return this.isSolid() && result;
    } // И вот я сижу и понимаю как это некрасиво

    public boolean isCollidedWith(Physical o) {
        if ( o == this )
            return false;
        float hw = o.physWidth /2f; //      halfWidth
        float hh = o.physHeight/2f; // and  halfHeight
        float hwn= this.physWidth / 2f;
        float hhn= this.physHeight/ 2f;
        boolean isCollide = (
                Math.abs(this.x - o.x) <= hw + hwn &&
                Math.abs(this.y - o.y) <= hh + hhn);
        return isCollide;
    }


                // events
    protected void onCollide (){

    }

    protected void onCollideWith (Physical o) {

    }

    public void addForce( Vector2f force ){
        this.force.x += force.x;
        this.force.y += force.y;
    }
    public void addForce( float x, float y  ){
        this.force.x += x;
        this.force.y += y;
    }

    public void setForce ( Vector2f force ) {
        this.force = force;
    }
    public void setForce ( float x, float y ) {
        this.force.x = x;
        this.force.y = y;
    }
    public void setForceX(float x) {
        this.force.setX(x);
    }
    public void setForceY(float y) {
        this.force.setY(y);
    }

    public Vector2f getForce() {
        return force;
    }
    public float getForceX() {
        return force.x;
    }
    public float getForceY() {
        return force.y;
    }

    public boolean dotIn ( float x, float y) {
        return
                x > this.x - this.physWidth /2f &&
                x < this.x + this.physWidth /2f &&
                y > this.y - this.physHeight/2f &&
                y < this.y + this.physHeight/2f ;
    }

    // if any vertices of other object in square of this object
    // @return true


    public Set<Physical> getCollisions() {
        return collisions;
    }

    public Vector2f getGravityForce() {
        return gravityForce;
    }

    public void setGravityForce(Vector2f gravityForce) {
        this.gravityForce = gravityForce;
        this.gravityForceInitial.set(this.gravityForce);
    }
    public void setGravityForce(float x, float y) {
        this.gravityForce.x = x;
        this.gravityForce.y = y;
        this.gravityForceInitial.set(this.gravityForce);
    }

    public boolean isGravityEnabled() {
        return gravityEnabled;
    }

    public void setGravityEnabled(boolean gravityEnabled) {
        this.gravityEnabled = gravityEnabled;
    }
    public float getPhysWidth() {
        return physWidth;
    }

    public void setPhysWidth(float physWidth) {
        this.physWidth = physWidth;
    }

    public float getPhysHeight() {
        return physHeight;
    }

    public void setPhysHeight(float physHeight) {
        this.physHeight = physHeight;
    }

    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean solid) {
        this.solid = solid;
    }
}
