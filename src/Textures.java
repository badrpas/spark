import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *  Created by Badrpas  on 05.08.14.
 */

public final class Textures {

    private Textures(){
    }

    static Map <String, Texture> textures = new HashMap<String, Texture>();

    public static boolean load ( String name ) {
        System.out.print("Trying load new texture: " + name + "\n");
        Texture texture;
        try {
            texture = TextureLoader.getTexture("PNG", ResourceLoader.getResourceAsStream(name));
        } catch (IOException e) {
            return false;
        }
        textures.put( name, texture );
        System.out.print("Added new texture: " + name + "\n");
        return true;
    }

    public static Texture get ( String name ) {
        if (textures.containsKey( name ))
            return textures.get( name );
        load( name );
        return textures.get( name );
    }

}