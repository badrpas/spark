import org.newdawn.slick.opengl.Texture;
import static org.lwjgl.opengl.GL11.*;

public final class Render {
    private Render(){}

    // Draws texture with given coords on it
    public static void draw(Texture texture,
                            float x1, float y1,
                            float x2, float y2,
                            float inner_x1, float inner_y1,
                            float inner_x2, float inner_y2){
        if (texture == null)
            return;
        texture.bind();
        glBegin(GL_QUADS);
            glTexCoord2f    (inner_x1, inner_y1);
            glVertex2f      ( x1,  y1);
            glTexCoord2f    (inner_x2, inner_y1);
            glVertex2f      ( x2,  y1);
            glTexCoord2f    (inner_x2, inner_y2);
            glVertex2f      ( x2,  y2);
            glTexCoord2f    (inner_x1, inner_y2);
            glVertex2f      ( x1,  y2);
        glEnd();
    }
    public static void texture( Texture texture,
                               float x1, float y1,
                               float x2, float y2) {
        draw(texture, x1, y1, x2, y2, 0, 0, 1, 1);
    }
    public static void animation ( Animation a,
                                   float x1, float y1,
                                   float x2, float y2) {
        if ( a == null ) {
            return;
        }
        if ( a.getFrameCount() == 0)
            return;
        //a.getDrawable().render(x1, y1, x2, y2);
        draw(a.getTexture(), x1, y1, x2, y2, a.getX1(), a.getY1(), a.getX2(), a.getY2());
    }
    public static void texture(String name,
                               float x1, float y1,
                               float x2, float y2){
        draw(Textures.get(name), x1, y1, x2, y2, 0, 0, 1, 1);
    }
    public static void texture(String name,
                               float x1, float y1,
                               float x2, float y2,
                               float inner_x1, float inner_y1,
                               float inner_x2, float inner_y2){
        draw(Textures.get(name), x1, y1, x2, y2, inner_x1, inner_y1, inner_x2, inner_y2);
    }

}

























