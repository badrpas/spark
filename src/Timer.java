/**
 * Created by Badrpas on 04.08.14.
 *
 */
import org.lwjgl.Sys;

public final class Timer {
    private static     float   dt  = 0;
    private static     long    pre = 0;
    private static     long    cur = 0;
    public static void update() {
        if (pre == 0)
            pre = Sys.getTime();
        else
            pre = cur;
        cur = Sys.getTime();
        dt = ( cur - pre ) / 1000.0f;
    }
    public static float dt() {
        return dt;
    }
    Timer() {}
}
