import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;

import static org.lwjgl.opengl.GL11.*;

public final class Engine {
    boolean     terminated  = false;
    Animations  animations;
    Scene       scene;

    private void setupResolution ( int w, int h ) {
        try {
            Display.setDisplayMode( new DisplayMode( w, h ) ); // Размеры окна
        } catch (LWJGLException e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    public void initGL () {
        glEnable        (GL_TEXTURE_2D);
        //glEnable        (GL_DEPTH_TEST);
        glEnable        (GL_BLEND);
        glBlendFunc     (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glMatrixMode    (GL_PROJECTION);
        glLoadIdentity  ();
        glOrtho         (0, Display.getWidth(), Display.getHeight(), 0, 1.0d, 2.0d);
        glMatrixMode    (GL_MODELVIEW);
        glClearColor    (1.0f, 1.0f, 1.0f, 1.0f);
    }

    void load() {
        animations = new Animations();
        animations.add( new Animation( "counts"     , "res\\counts.png", 32, 32, 0, 9, 1.0f  ));
        animations.add( new Animation( "run"        , "res\\manRun.png", 32, 32, 0, 11, 30f  ));
        animations.add( new Animation( "runSlow"    , "res\\manRun.png", 32, 32, 0, 11, 20f  ));
        animations.add( new Animation( "villager"   , "res\\VillagerRun1.png", 128, 128, 91, 13, 20.0f));
        animations.add( new Animation( "idle"       , "res\\box1.png", 32, 32, 0, 1, 1f));

        scene = new Scene();
        Entity.setCurrentScene(scene);
        new Player ();
//        new BoxTest();
        float y = 600-16-64;
        for ( x = 16; x <= 800-16; x+=32) {
            new Box(x, y);
        }
        new Box(16,400);
        new Box(48,400);
        new Box(80,400);
        new Box(716,400);
        new Box(784,400);
        new Box(128+64, 420);

    }

    float       x           =   400;
    float       y           =   100;
    int         hWidth      =   256;
    int         hHeight     =   256;

    float runnerX = 0.0f;
    float runnerSpeed = 200.0f;

    public void render() {
        glClear(GL_COLOR_BUFFER_BIT);
        Render.texture(Textures.get("res\\hadacofee.png"), x, y, x + hWidth, y + hHeight);
        animations.updateAll();
        Render.animation(animations.get("villager"), runnerX, Display.getHeight()-64, runnerX+64, Display.getHeight());
        animations.get("counts_").render( 100+64, 100+64, 164+64, 164+64 );
        scene.renderAll();
    }

    public void update() {
        if ( Display.wasResized() ) {
            glViewport          (0,0, Display.getWidth(), Display.getHeight() );
            glMatrixMode        (GL_PROJECTION);
            glLoadIdentity      ();
            glOrtho             (0,   Display.getWidth(), Display.getHeight(), 0, 100, -1);
            glMatrixMode        (GL_MODELVIEW);
        }
        Timer.update();
        runnerX += runnerSpeed * Timer.dt();
        if ( (int)runnerX > Display.getWidth() )
            runnerX = -32.0f;
        updateKeyboard();
        if (Display.isCloseRequested())
            terminated = true;
        if (Mouse.isButtonDown(0) ) {
            System.out.print("");
        }
        scene.updateAll();
    }

    void updateKeyboard() {
        if ( Keyboard.isKeyDown ( Keyboard.KEY_ESCAPE ) )
            terminated = true;
    }


    Engine () {
        setupResolution( 800, 600 );
        try {
            Display.create();
        } catch (LWJGLException e) {
            return;
        }
        Display.setResizable(true);
        Display.setTitle( "Я рисую как дибил" );
        initGL();
        load();
    }

    public void start() {
        while ( !terminated ) {
            update();
            render();
            Display.update();
            Display.sync(60);
        }
    }

    public static void main ( String [] argv ) {
        System.out.print( "Hello there!\n:D\n" );
        Engine engine = new Engine();
        engine.start();
    }
}
