import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;

public final class Animation {
    private String      name;
    private String      fileName;
    private Texture     texture;
    private int         width;
    private int         height;
    private int         startFrame;
    private int         frameCount;
    private int         currentFrame;
    private float       speed;
    private float       life;
    private float       x1, y1;
    private float       x2, y2;
    private Drawable    drawable;
    private boolean     reversedHor;
    private boolean     reversedVert;

    Animation ( String animationName, String textureName, int width, int height, int start, int count, float fps ) {
        this.texture        = Textures.get(textureName);
        this.texture.setTextureFilter(GL11.GL_NEAREST);
        this.width          = width;
        this.height         = height;
        this.startFrame     = start;
        this.frameCount     = count;
        this.speed          = fps;
        this.life           = 0.0f;
        this.currentFrame   = 0;
        this.name           = animationName;
        this.fileName       = textureName;
        this.reversedHor    = false;
        this.reversedVert   = false;
        updatePos();
        this.drawable       = new Drawable( texture, x1, y1, x2, y2 );

        //Animations.add      (this.name, this);
    }

    @Override
    public String toString() {
        return name +"@" + fileName;
    }

    public void update() {
        if (frameCount < 1 )
            return;
        else if ( frameCount == 1) {
            updatePos();
            return;
        }
        life += Timer.dt();
        if (life  > 1f/speed * frameCount )
            life -= 1f/speed * frameCount;
        currentFrame = (int) (life / (1f/speed));
        updatePos();
    }

    void updatePos () {
        int n = currentFrame + startFrame;
        int x = n % ( texture.getImageWidth() / width );
        int y = n / ( texture.getImageWidth() / width );
        x1 = (float)x *  width  / texture.getTextureWidth();
        x2 = x1 + (float)width  / texture.getTextureWidth();
        y1 = (float)y *  height / texture.getTextureHeight();
        y2 = y1 + (float)height / texture.getTextureHeight();
        if ( this.isReversedHor() ) {
            float t = x1;
            x1 = x2;
            x2 = t;
        }
        if ( this.isReversedVert() ) {
            float t = y1;
            y1 = y2;
            y2 = t;
        }
    }

    public void render (float x1, float y1,
                        float x2, float y2 ){
        Render.animation ( this, x1, y1, x2, y2 );
    }

    public boolean isReversedVert() {
        return reversedVert;
    }

    public void setReversedVert(boolean reversedVert) {
        this.reversedVert = reversedVert;
    }

    public boolean isReversedHor() {
        return reversedHor;
    }

    public void setReversedHor(boolean reversedHor) {
        this.reversedHor = reversedHor;
    }

    public String getName() {
        return name;
    }

    public int getFrameCount() {
        return frameCount;
    }

    public Texture getTexture() {
        return texture;
    }

    public String getFileName() {
        return fileName;
    }

    public int getStartFrame() {
        return startFrame;
    }

    public int getCurrentFrame() {
        return currentFrame;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public Drawable getDrawable() {
        drawable.setCoords(x1, y1, x2, y2);
        return drawable;
    }

    public float getX1() {
        return x1;
    }

    public float getY1() {
        return y1;
    }

    public float getX2() {
        return x2;
    }

    public float getY2() {
        return y2;
    }
}
