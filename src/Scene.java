import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Badrpas on 09.08.2014.
 */
public class Scene {
    private Set< Entity > objects;
    Scene () {
        objects = new HashSet<Entity>();
    }
    public void addEntity ( Entity e ) {
        objects.add(e);
    }
    public void updateAll() {
        for (Entity object : objects) {
            object.preUpdate();
        }
        for (Entity object : objects) {
            object.update();
        }
        for (Entity object : objects) {
            object.postUpdate();
        }
    }
    public void renderAll(){
        for (Entity object : objects) {
            object.render();
        }
    }
}
