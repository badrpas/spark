import java.util.HashMap;
import java.util.Map;

/**
 *  Created by Badrpas on 06.08.14.
 */

public final class Animations {
    private static Animation nothing = new Animation("none", "res\\none.png", 1, 1, 0, 0, 1);

    private Map <String, Animation> animations ;

    Animations () {
        animations = new HashMap<String, Animation>();
    }
    public boolean contains ( String name ) {
        if (animations.containsKey(name))
            return true;
        else
            return false;
    }
    public void add (Animation a) {
        animations.put(a.getName(), a);
    }

    public Animation get ( String name ) {
        if (animations.containsKey(name))
            return animations.get( name );
        else
            return nothing;
    }

    public void updateAll () {
        for ( Animation a : animations.values() ) {
            a.update();
        }
    }
}
