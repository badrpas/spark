import org.newdawn.slick.opengl.Texture;

/**
 *  Created by Badrpas on 09.08.2014.
 */

// Kinda useless class tbh
public final class Drawable {

    private float       x1, y1;
    private float       x2, y2;
    private Texture     texture;
    public void render (float x1, float y1,
                        float x2, float y2) {
        Render.draw(texture, x1, y1, x2, y2, this.x1, this.y1, this.x2, this.y2);
    }

    Drawable ( Texture t,
               float x1, float y1,
               float x2, float y2 ) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.texture = t;
    }

    public void setCoords (
            float x1, float y1,
            float x2, float y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }
}
